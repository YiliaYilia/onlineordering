package com.cxj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cxj.dataObject.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, String>{
	List<OrderDetail>findByOrderId(String orderId);

}
