package com.cxj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cxj.dataObject.ProductCategory;

/**
 * dao层
 * @author XiuJuanChang
 * JpaRepository<实体类, 主键类型>
 */
public interface ProductCategoryRepository  extends JpaRepository<ProductCategory, Integer>{

	
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);





}
