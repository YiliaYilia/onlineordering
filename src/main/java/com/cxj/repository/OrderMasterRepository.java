package com.cxj.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cxj.dataObject.OrderMaster;

public interface OrderMasterRepository  extends JpaRepository<OrderMaster, String>{

	Page<OrderMaster> findByBuyerOpendid(String buyerOpendid,Pageable pageable);
}
