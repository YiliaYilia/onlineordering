package com.cxj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cxj.dataObject.ProductInfo;

public interface ProductInfoRepository extends JpaRepository<ProductInfo, String>{
    List<ProductInfo> findByProductStatus(Integer productStatus);

}
