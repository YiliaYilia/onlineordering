package com.cxj.exception;

import com.cxj.enums.ResultEnum;

public class SellException extends RuntimeException{
	 private Integer code;
	 
	 public SellException(ResultEnum re) {
		 super(re.getMessage());
		 this.code=re.getCode();
	 }
}
