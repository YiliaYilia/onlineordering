package com.cxj.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cxj.dataObject.ProductCategory;
import com.cxj.dataObject.ProductInfo;
import com.cxj.service.CategoryService;
import com.cxj.service.ProductService;
import com.cxj.util.ResultVOUtil;
import com.cxj.vo.ProductInfoVO;
import com.cxj.vo.ProductVO;
import com.cxj.vo.ResultVo;

/**
 * 买家商品
 * @author XiuJuanChang
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
//	private static Logger logger =Logger.getLogger(BuyerProductController.class);
	//查询商品
	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService  categoryservice;
	@GetMapping("list")
	public ResultVo<ProductVO>  list() {
	//1.查询所有上架商品
	List <ProductInfo>productinfoList =productService.findUPAll();
	//2.查询类目《一次性查询全部》-性能
	List <Integer>categoryTypeList=new ArrayList<>();
	for (ProductInfo productinfo : productinfoList) {
		categoryTypeList.add(productinfo.getProductType());
		System.err.println(productinfo);
	}
	
	 List<ProductCategory> productCategoryList = categoryservice.findByCategoryTypeIn(categoryTypeList);
	//3.数据拼装
	 List<ProductVO> productVOList = new ArrayList<>();
	for (ProductCategory productcategory :productCategoryList) {
		ProductVO pv =new ProductVO();
		pv.setCategoryType(productcategory.getCategoryType());
		pv.setCategoryName(productcategory.getCategoryName());
		List<ProductInfoVO> productInfoVOList = new ArrayList<>();
		
		for (ProductInfo productInfo: productinfoList) {
			if(productInfo.getProductType().equals(productcategory.getCategoryType())) {
				ProductInfoVO productInfoVO = new ProductInfoVO();
				BeanUtils.copyProperties(productInfo, productInfoVO);
				productInfoVOList.add(productInfoVO);
			}
		}
		pv.setProductInfoVOList(productInfoVOList);
		productVOList.add(pv);
		
	}
//    logger.error("Did it again!");   //error级别的信息，参数就是你输出的信息
//    logger.info("我是info信息");    //info级别的信息
//    logger.debug("我是debug信息");
//    logger.warn("我是warn信息");
//    logger.fatal("我是fatal信息");
//    logger.log(Level.DEBUG, "我是debug信息");    

		return ResultVOUtil.success(productVOList);
	}
	@PostMapping("/user")
	public String Login(String json){
	    System.out.println(json.toString());
	    String msg = "1";
	    return msg; 
	}
}
