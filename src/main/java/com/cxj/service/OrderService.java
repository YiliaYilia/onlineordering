package com.cxj.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cxj.dto.OrderDTO;

public interface OrderService {
	/**
	 * 1.创建订单
	 * 2.查询单个订单 
	 * 3.查询订单列表
	 * 4.取消订单
	 * 5.完结订单
	 * 6.支付订单
	 * 4.5.6修改订单状态操作
	 */
	OrderDTO create(OrderDTO orderdto);
	OrderDTO findOne(String orderId);
	Page <OrderDTO> findList(String buyerOpendid,Pageable pageable);
	OrderDTO cancel(OrderDTO orderdto);
	OrderDTO finish(OrderDTO orderdto);
	OrderDTO paid(OrderDTO orderdto);
}
