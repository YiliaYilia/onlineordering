package com.cxj.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.cxj.converter.OrderMaster2OrderDTOConverter;
import com.cxj.dataObject.OrderDetail;
import com.cxj.dataObject.OrderMaster;
import com.cxj.dataObject.ProductInfo;
import com.cxj.dto.CartDTO;
import com.cxj.dto.OrderDTO;
import com.cxj.enums.OrderStatusEnum;
import com.cxj.enums.PayStatusEnum;
import com.cxj.enums.ResultEnum;
import com.cxj.exception.SellException;
import com.cxj.repository.OrderDetailRepository;
import com.cxj.repository.OrderMasterRepository;
import com.cxj.service.OrderService;
import com.cxj.service.PayService;
import com.cxj.service.ProductService;
import com.cxj.util.KeyUtil;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class OrderServiceImpl implements OrderService{

	 @Autowired
	 private ProductService productService;
	 @Autowired
	 private OrderDetailRepository orderdetailrepository;
	// @Autowired
	 //private PayService payService;
    @Autowired
    private OrderMasterRepository orderMasterRepository;

	 String orderid  =KeyUtil.genUniqueKey();
	@Transactional
	@Override
	public OrderDTO create(OrderDTO orderdto) {
		BigDecimal bd=new BigDecimal(BigInteger.ZERO);
		//查询商品
		for(OrderDetail od :orderdto.getOrderDetailList()) {
			ProductInfo proInfo=productService.findOne(od.getProductId());
			if(proInfo==null) {
				throw new  SellException(ResultEnum.ORDER_NOT_EXIST);
			}
			//计算订单总价
		 	bd= proInfo.getProductPrice().multiply(new BigDecimal(od.getProductQuantity())).add(bd);
			//订单详情入库
			
			od.setOrderId(orderid);
			od.setDetailId(KeyUtil.genUniqueKey());
			BeanUtils.copyProperties(proInfo,od);
			orderdetailrepository.save(od);
		}
		
		/**
		 * 写入订单数据库<orderMaster,orderDetail>
		 */
		//3. 写入订单数据库（orderMaster和orderDetail）
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderdto, orderMaster);
        orderdto.setOrderId(orderid);
        
        orderMaster.setOrderAmount(bd);
        orderMaster.setOrderStatus(OrderStatusEnum.NEW.getCode());
        orderMaster.setPayStatus(PayStatusEnum.WAIT.getCode());
        orderMasterRepository.save(orderMaster);
		//扣库存
        List<CartDTO> cartDTOList = orderdto.getOrderDetailList().stream().map(e ->
        new CartDTO(e.getProductId(), e.getProductQuantity())
		).collect(Collectors.toList());
		productService.decreaseStock(cartDTOList);
		return orderdto;
	}

	@Override
	public OrderDTO findOne(String orderId) {
		  OrderMaster orderMaster = orderMasterRepository.findById(orderId).get();
		 
	        if (orderMaster == null) {
	            throw new SellException(ResultEnum.ORDER_NOT_EXIST);
	        }

	        List<OrderDetail> orderDetailList = orderdetailrepository.findByOrderId(orderId);
	        if (CollectionUtils.isEmpty(orderDetailList)) {
	            throw new SellException(ResultEnum.ORDERDETAIL_NOT_EXIST);
	        }

	        OrderDTO orderDTO = new OrderDTO();
	        BeanUtils.copyProperties(orderMaster, orderDTO);
	        orderDTO.setOrderDetailList(orderDetailList);

	        return orderDTO;
	}

	@Override
	public Page<OrderDTO> findList(String buyerOpendid, Pageable pageable) {
		 Page<OrderMaster> orderMasterPage = orderMasterRepository.findByBuyerOpendid(buyerOpendid, pageable);

	        List<OrderDTO> orderDTOList = OrderMaster2OrderDTOConverter.convert(orderMasterPage.getContent());

	        return new PageImpl<OrderDTO>(orderDTOList, pageable, orderMasterPage.getTotalElements());
	}

	@Override
	@Transactional
	public OrderDTO cancel(OrderDTO orderdto) {
		 OrderMaster orderMaster = new OrderMaster();
		 BeanUtils.copyProperties(orderdto, orderMaster);
	        //判断订单状态
	        if (!orderdto.getOrderStatus().equals(OrderStatusEnum.NEW.getCode())) {
	            log.error("【取消订单】订单状态不正确, orderId={}, orderStatus={}", orderdto.getOrderId(), orderdto.getOrderStatus());
	            throw new SellException(ResultEnum.ORDER_STATUS_ERROR);
	        }

	        //修改订单状态
	        orderdto.setOrderStatus(OrderStatusEnum.CANCEL.getCode());//已取消
	       // BeanUtils.copyProperties(orderdto, orderMaster);
	        OrderMaster updateResult = orderMasterRepository.save(orderMaster);
	        if (updateResult == null) {
	            log.error("【取消订单】更新失败, orderMaster={}", orderMaster);
	            throw new SellException(ResultEnum.ORDER_UPDATE_FAIL);
	        }

	        //返回库存
	        if (CollectionUtils.isEmpty(orderdto.getOrderDetailList())) {
	            log.error("【取消订单】订单中无商品详情, orderDTO={}", orderdto);
	            throw new SellException(ResultEnum.ORDER_DETAIL_EMPTY);
	        }
	        List<CartDTO> cartDTOList = orderdto.getOrderDetailList().stream()
	                .map(e -> new CartDTO(e.getProductId(), e.getProductQuantity()))
	                .collect(Collectors.toList());
	        productService.increaseStock(cartDTOList);

	        //如果已支付, 需要退款
	        if (orderdto.getPayStatus().equals(PayStatusEnum.SUCCESS.getCode())) {
	           // payService.refund(orderdto);
	        	//TODO
	        }

	        return orderdto;
	}

	@Override
	public OrderDTO finish(OrderDTO orderdto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OrderDTO paid(OrderDTO orderdto) {
		// TODO Auto-generated method stub
		return null;
	}

}
