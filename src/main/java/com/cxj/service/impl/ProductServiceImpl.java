package com.cxj.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cxj.dataObject.ProductInfo;
import com.cxj.dto.CartDTO;
import com.cxj.enums.ProductStatusEnum;
import com.cxj.enums.ResultEnum;
import com.cxj.exception.SellException;
import com.cxj.repository.ProductInfoRepository;
import com.cxj.service.ProductService;

@Service
public class ProductServiceImpl  implements ProductService{

	@Autowired
	private ProductInfoRepository pr;
	
	@Override
	public ProductInfo findOne(String productId) {
		 
		return pr.getOne(productId);
	}

	@Override
	public Page<ProductInfo> findAll(Pageable pageable) {
		return pr.findAll(pageable);
	}

	@Override
	public List<ProductInfo> findUPAll() {
		return pr.findByProductStatus(ProductStatusEnum.UP.getCode()) ;
	}

//    @Override
//    public List<ProductInfo> findUpAll() {
//        return repository.findByProductStatus(ProductStatusEnum.UP.getCode()).stream()
//                .map(e -> e.addImageHost(upYunConfig.getImageHost()))
//                .collect(Collectors.toList());
//    }

	@Override
	public ProductInfo save(ProductInfo productInfo) {
		return pr.save(productInfo);
	}

	@Override
	@Transactional
	public void increaseStock(List<CartDTO> cartdtoList) {
		for (CartDTO cartDTO: cartdtoList) {
            ProductInfo productInfo = pr.findById(cartDTO.getProductId()).get();
            if (productInfo == null) {
                throw new SellException(ResultEnum.PRODUCT_NOT_EXIST);
            }
            Integer result = productInfo.getProductStock() + cartDTO.getProductQuantity();
            productInfo.setProductStock(result);

            pr.save(productInfo);
        }
	}
	@Transactional
	@Override
	public void decreaseStock(List<CartDTO> cartdtoList) {
		for(CartDTO cartdto:cartdtoList) {
			ProductInfo productinfo =pr.findById(cartdto.getProductId()).get();
			if(productinfo==null) {
				throw new SellException(ResultEnum.PRODUCT_NOT_EXIST);
			}
			Integer result =productinfo.getProductStock()-cartdto.getProductQuantity();
			if (result<0) {
				throw new SellException(ResultEnum.ORDER_STATUS_ERROR);
			}
			productinfo.setProductStock(result);
			pr.save(productinfo);
		}
	}

}
