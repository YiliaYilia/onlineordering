package com.cxj.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cxj.dataObject.ProductInfo;
import com.cxj.dto.CartDTO;

public interface ProductService {
	
	
	ProductInfo findOne  (String productId);
	/**
	 * 查询所有在架 的商品
	 * @return
	 */
	List <ProductInfo> findUPAll();
	
	Page<ProductInfo> findAll(Pageable pageable);
	
	ProductInfo save(ProductInfo productInfo);
	
	 /**
	  * +库存
	  */
	void increaseStock(List<CartDTO>cartdtoList);
	
	/**
	 * -库存
	 */
	void decreaseStock(List<CartDTO>cartdtoList);
}
