package com.cxj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Springboot 启动类
 * 
 * @author XiuJuanChang
 *
 */
@SpringBootApplication
//@MapperScan(basePackages = "com.cxj.dataobject.mapper")

@EnableCaching
public class MyApp {
	public static void main(String[] args) throws Exception {
 
		SpringApplication.run(MyApp.class, args);
	}
}
