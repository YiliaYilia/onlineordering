package com.cxj.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import com.cxj.dataObject.OrderDetail;

import lombok.Data;

@Data
public class OrderDTO {
	private String orderId;
	private String buyerName;
	private String buyerPhone;
	private String buyerAddress;
	private String buyerOpendid;
	private BigDecimal orderAmount;
	private Integer orderStatus;
	/** 支付状态, 默认为0未支付. */
    private Integer payStatus;
	private Date createTime;
	private Date updateTime;
	private List<OrderDetail>orderDetailList;
	 
}
