package com.cxj.enums;
/**
 * 单例
 * @author XiuJuanChang
 *
 */
	public enum EnumDemo {
	    A;
	    public void doSomeThing() {
	        System.out.println("dosomething");
	    }

	    public static void main(String[] args) {
	        EnumDemo.A.doSomeThing();
	    }
	}
