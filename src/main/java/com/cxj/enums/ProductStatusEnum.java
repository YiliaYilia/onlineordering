package com.cxj.enums;

import lombok.Getter;

/**
 * 枚举类
 * 区分商品状态
 * @author XiuJuanChang
 *
 */
@Getter
public enum ProductStatusEnum implements CodeEnum{
	UP(0,"在售"),
	DOWN(1,"下架");
	
	private Integer code;
	private String message;
	
	private ProductStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	
	
}
