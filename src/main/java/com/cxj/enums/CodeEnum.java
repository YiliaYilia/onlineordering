package com.cxj.enums;

/**
 * 
 * @author XiuJuanChang
 *
 */
public interface CodeEnum {
    Integer getCode();
}
