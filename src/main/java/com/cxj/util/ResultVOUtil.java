package com.cxj.util;

import com.cxj.vo.ResultVo;
/**
 * 返回的场景
 * @author XiuJuanChang
 *
 */
public class ResultVOUtil {
	public static ResultVo success (Object obj) {
		ResultVo result =new ResultVo();
		result.setCode(1);
		result.setMsg("成功");
		result.setData(obj);
		return result;
	}
	public static ResultVo success() {
		return null;
	}
	public static ResultVo error(Integer resultCode,String msg) {
		ResultVo result =new ResultVo();
		result.setCode(resultCode);
		result.setMsg(msg);
		return result;
	}
}
