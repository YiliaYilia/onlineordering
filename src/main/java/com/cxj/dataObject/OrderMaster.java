package com.cxj.dataObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.cxj.enums.OrderStatusEnum;
import com.cxj.enums.PayStatusEnum;

import lombok.Data;

@Entity
@Data
@DynamicUpdate
public class OrderMaster {
	@Id
	private String orderId;
	private String buyerName;
	private String buyerPhone;
	private String buyerAddress;
	private String buyerOpendid;
	private BigDecimal orderAmount;
	private Integer orderStatus=OrderStatusEnum.NEW.getCode();
	private Integer payStatus=PayStatusEnum.WAIT.getCode();
	private Date createTime;
	private Date updateTime;
}
