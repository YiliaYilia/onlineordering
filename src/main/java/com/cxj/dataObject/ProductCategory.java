package com.cxj.dataObject;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

//import lombok.Data;
 

/**
 * 把数据库映射成对象，用注解方式
 * @author XiuJuanChang
 *
 */
@Entity
@DynamicUpdate
@Data
public class ProductCategory {
	@Id //主键
	@GeneratedValue (strategy = GenerationType.IDENTITY)//自增类型
	private Integer categoryId;//类目id
	private String categoryName;//类目名字
	private Integer categoryType;//类目类型
	private Date createTime;
	private Date updateTime;
	public  ProductCategory() {
		
	}
  

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "ProductCategory [categoryId=" + categoryId + ", categoryName=" + categoryName + ", categoryType="
				+ categoryType + "]";
	}
	

	    public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Integer getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(Integer categoryType) {
		this.categoryType = categoryType;
	}
		public ProductCategory(String categoryName, Integer categoryType) {
	        this.categoryName = categoryName;
	        this.categoryType = categoryType;
	    }
}
