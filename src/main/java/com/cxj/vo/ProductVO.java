package com.cxj.vo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 商品包含类目
 * @author XiuJuanChang
 *
 */
@Data
public  class ProductVO implements Serializable{
	private static final long serialVersionUID = 7097863777546530545L;
	@JsonProperty("name")
	private String categoryName;
	@JsonProperty("type")
	private Integer categoryType;
	@JsonProperty("foods")
	private List<ProductInfoVO> productInfoVOList;
	
	
}
