package com.cxj.repository;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cxj.MyApp;
import com.cxj.dataObject.ProductInfo;

@RunWith(value = SpringRunner.class)
@SpringBootTest(classes= {MyApp.class})
public class ProductInfoRepositoryTest {
	@Autowired
	private ProductInfoRepository repository ;
	@Test
	public  void  saveTest() {
		ProductInfo info =new ProductInfo();
		info.setProductId("11111");
		info.setProductName("心灵鸡汤");
		info.setProductPrice(new BigDecimal(3.2));
		info.setProductStock(10);
		info.setProductIcon("http://baidu.com");
		info.setProductType(3);
		info.setProductStatus(0);
		ProductInfo result=  repository.save(info);
		System.err.println(info);
		Assert.assertNotNull(result);
	}
	@Test
	public void findByProductStatus() {
		List <ProductInfo> list =repository.findByProductStatus(0);
		Assert.assertNotEquals(0, list.size());
	}

}
