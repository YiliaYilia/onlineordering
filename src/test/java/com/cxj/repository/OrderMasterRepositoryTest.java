package com.cxj.repository;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cxj.dataObject.OrderMaster;


@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMasterRepositoryTest {
	@Autowired
    private OrderMasterRepository repository;

    private final String OPENID = "0904";

    @Test
    public void saveTest() {
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("520");
        orderMaster.setBuyerName("小又又");
        orderMaster.setBuyerPhone("15021666943");
        orderMaster.setBuyerAddress("小胖子");
        orderMaster.setBuyerOpendid(OPENID);
        orderMaster.setOrderAmount(new BigDecimal(2.5));
        System.err.println(orderMaster);
        OrderMaster result = repository.save(orderMaster);
        Assert.assertNotNull(result);//不等于null就成功了 
        
    }
    @Test
    public void findByBuyerOpenid() throws Exception {
        PageRequest request = new PageRequest(0, 3);

        Page<OrderMaster> result = repository.findByBuyerOpendid(OPENID, request);

        Assert.assertNotEquals(0, result.getTotalElements());
    }

}