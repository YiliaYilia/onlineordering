package com.cxj.repository;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cxj.MyApp;
import com.cxj.dataObject.ProductCategory;
import com.cxj.dataObject.ProductInfo;
import com.cxj.service.ProductService;

import junit.framework.Assert;

/**
 * 
 * @author XiuJuanChang
 *
 */
@RunWith(value = SpringRunner.class)
@SpringBootTest(classes= {MyApp.class})
public class ProductCategoryRepositoryTest {

    @Autowired 
    private ProductCategoryRepository repository;
    @Autowired
	private ProductService productService;
    @Transactional
    @Test
    public void findOneTest() {
        ProductCategory productCategory = repository.getOne(1);
        System.out.println("productCategory:"+productCategory.toString());
    	System.err.println("查询库里id为1的数据");
    }
    @Test
    public void findUPAll() {
    	
    	List <ProductInfo>productinfoList =productService.findUPAll();
    	System.err.println(productinfoList);
    }
    @Test
    public void saveTest() {
    	ProductCategory productCategory=new ProductCategory();
    	productCategory.setCategoryId(2);
    	productCategory.setCategoryName("小可爱");
    	productCategory.setCategoryType(5);
    	repository.save(productCategory);
    	System.err.println("新插入数据"+productCategory);
    	
    }
    @Test
    @Transactional
   
    //service中，抛出异常就回滚
    //事务，在测试中，➕注解，方法啊完成后回滚数据
    public void updateTest() {
    	ProductCategory productCategory = repository.findById(1).get();
    	productCategory.setCategoryType(10);
    	productCategory.setCategoryName("小双双");
    	ProductCategory result  =repository.save(productCategory);
    	System.err.println("更新数据"+productCategory);
        Assert.assertNotNull(result);
    }
    @Test
    public void ProductCategoryRepository( ) {
    	List <Integer>list =Arrays.asList(3,4,5);
    	List<ProductCategory> result  =	repository.findByCategoryTypeIn(list);
     System.err.println(result.size());
     System.err.println(repository.findByCategoryTypeIn(list));
    }
}

