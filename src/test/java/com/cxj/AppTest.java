package com.cxj;
import javax.swing.Spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;



/**
 * 日志测试类
 * @author XiuJuanChang
 *
 */
@RunWith(value = SpringRunner.class)
@SpringBootTest(classes= {MyApp.class})
@Slf4j
public class AppTest{
	private final Logger logger=LoggerFactory.getLogger(AppTest.class);
	
	@Test
	public void test1() {
		String name="juanjuan";
		String age ="18";
		logger.info("name: {},age: {}", name,age);
		
		logger.debug("debug...");
		logger.info("info...");
		logger.error("error...");
	}
}
