package com.cxj.service;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;
import com.cxj.MyApp;
import com.cxj.dataObject.ProductInfo;
import com.cxj.enums.ProductStatusEnum;
import com.cxj.service.impl.ProductServiceImpl;


@Service				
@RunWith(value = SpringRunner.class)
@SpringBootTest(classes= {MyApp.class})
public class ProductServiceImplTest {

	@Autowired
	private ProductServiceImpl ps;
	@Test
	public void findOne() throws Exception{
		ProductInfo info =ps.findOne("12345");
		Assert.assertEquals("12345", info.getProductId());
	}
	@Test
	public void findAll() throws Exception{
		PageRequest req=new PageRequest(0,2);
		Page<ProductInfo>page=	ps.findAll(req);
		Assert.assertNotEquals(0, page.getSize());
		System.out.println(page.getTotalElements());
	}
	@Test
	public void findUpAll() throws Exception{
		List <ProductInfo>productInfoList=ps.findUPAll();
	//	Assert.assertNotEquals(0, productInfoList.size());
	}
	@Test
	public void save() throws Exception{
		ProductInfo info=new ProductInfo();
		info.setProductId("22222");
		info.setProductName("哈尼");
		info.setProductPrice(new BigDecimal(2.3));
		info.setProductStock(20);
		info.setProductDescription("还不错");
		info.setProductIcon("http:baidu.com");
		info.setProductStatus(ProductStatusEnum.DOWN.hashCode());
		info.setProductType(23);
		ProductInfo result=	ps.save(info);
		 Assert.assertNotNull(result);
	}
}
