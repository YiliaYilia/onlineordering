package com.cxj.service;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import com.cxj.MyApp;
import com.cxj.dataObject.ProductCategory;
import com.cxj.service.impl.CategoryServiceImpl;

@Service				
@RunWith(value = SpringRunner.class)
@SpringBootTest(classes= {MyApp.class})
public class CategoryServiceImplTest {
	
	@Autowired
	private CategoryServiceImpl categoryServiceImpl;
	
	@Test
	public void findOneTest()  throws Exception{
		ProductCategory productCategory=categoryServiceImpl.findOne(1);
		//Assert.assertEquals(new Integer (1), productCategory.getCategoryId());
		System.err.println("findOneTest:"+productCategory);
	}
	@Test
	public void findAllTest() throws Exception{
		List result= categoryServiceImpl.findAll( );
		//Assert.assertEquals(0, result.size());
		System.err.println("findAllTest:"+result);
		
	}
	
	 @Test
	    public void findByCategoryTypeInTest() throws Exception {
	        List<ProductCategory> productCategoryList = categoryServiceImpl.findByCategoryTypeIn(Arrays.asList(1,2,3,4));
	       // Assert.assertNotEquals(0, productCategoryList.size());
	        System.err.println("findByCategoryTypeInTest:"+productCategoryList.size());
	    }

//	    @Test
//	    public void saveTest() throws Exception {
//	        ProductCategory productCategory = new ProductCategory("小可爱", 1);
//	        ProductCategory result = categoryServiceImpl.save(productCategory);
//	        //Assert.assertNotNull(result);
//	        System.err.println("saveTest:"+result);
//	    }
}
